import { Injectable } from '@angular/core';
import { IProduct } from './iproduct';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, first, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ProductService {
    private productsUrl = 'api/products/products.json';

    constructor(private httpClient: HttpClient) {
    }

    getProducts(): Observable<IProduct[]> {
        return this.httpClient.get<IProduct[]>(this.productsUrl).pipe(
            tap(data => console.log('All: ' + JSON.stringify(data))),
            catchError(this.handleError)
        );
    }

    private handleError(error: HttpErrorResponse){
        let errorMessage = '';

        if (error.error instanceof ErrorEvent) {
            errorMessage = `Error: ${error.error.message}`;
        }
        else {
            errorMessage = `Server returned: ${error.status} and message ${error.message}`;
        }

        console.error(errorMessage);
        return throwError(errorMessage);
    }
}
