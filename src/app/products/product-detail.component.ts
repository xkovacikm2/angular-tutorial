import { Component, OnInit } from '@angular/core';
import { IProduct } from './iproduct';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from "./product.service";

@Component({
    templateUrl: './product-detail.component.html',
    styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
    pageTitle = 'Product Detail';
    product: IProduct;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private productService: ProductService) {}

    ngOnInit() {
        let id = +this.route.snapshot.paramMap.get('id');
        this.pageTitle += `: ${id}`;
        this.productService.getProducts().subscribe({
                next: products => this.product = products[0]
            }
        );
    }

    onBack(): void {
        this.router.navigate(['/products']);
    }
}
